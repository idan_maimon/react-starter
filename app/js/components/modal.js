var React = require("react");
var Modal = require('react-modal-component');
 
var ModalComponent = React.createClass({
  getInitialState: function() {
    return { showModal: false };
  },
  openModal: function() {
    this.setState({showModal: true});
    if(this.props.onOpen) this.props.onOpen();
  },
  closeModal: function() {
    this.setState({showModal: false});
    if(this.props.onClose) this.props.onClose();
  },
  render: function() {
    var node = [];
    if (this.state.showModal) {
      node = (
        <Modal transitionName='fade'>
          <div>{this.props.children}</div>
          <button className="mar-top-15" onClick={this.closeModal}>Sort !</button>
        </Modal>
      )
    }
 
    return <div className="mar-top-15"><button onClick={this.openModal}>{this.props.buttonTitle}</button>{node}</div> 
  }
});

module.exports = ModalComponent;