var React = require("react");
var Modal = require("./modal");
var MySelect = require("./select");

var Table = React.createClass({
	getInitialState:function() {
		return {
			asc:true,
			primarySort:"categoryId",
			secondarySort:"",
			complexSort:false
		}
	},
	componentWillReceiveProps:function(nextProps) {
		// this.setState({
		// 	asc:true,
		// 	primarySort:"categoryId",
		// 	secondarySort:"",
		// 	complexSort:false	
		// });
	},
	onClick:function(e) {
		this.setState({
			primarySort: e.target.id,
			secondarySort:"",
			asc: !this.state.asc
		});
	},
	onMouseOver:function(e) {
		console.log(e.target.parentElement.nodeName);
		if(this.props.onRowHover) {
			this.props.onRowHover(e);
		}
	},
	onClose:function() {
		this.setState({
			complexSort:true,
			primarySort:this.refs.primarySelect.state.value,
			secondarySort:this.refs.secondarySelect.state.value,
		});
	},
	complexSort:function() {
		var primary = this.state.primarySort;
		var secondary = this.state.secondarySort;
		var data = this.props.data && this.props.data.length && this.props.data.slice() || [];
		if(primary) {
			data.sort(function(a,b) {
				if(primary && secondary) {
					if(this.state.asc) {
						if(a[primary] === b[primary])
					    {
					        var x = a[secondary].toLowerCase ? a[secondary].toLowerCase() : a[secondary], 
					        	y = b[secondary].toLowerCase ? b[secondary].toLowerCase() : b[secondary];
					        return x < y ? -1 : x > y ? 1 : 0;
					    }
					    return a[primary] - b[primary];
					}
					else {
						if(a[primary] === b[primary])
					    {
					        var y = a[secondary].toLowerCase ? a[secondary].toLowerCase() : a[secondary], 
					        	x = b[secondary].toLowerCase ? b[secondary].toLowerCase() : b[secondary];
					        return x < y ? -1 : x > y ? 1 : 0;
					    } 
					    return b[primary] - a[primary];	
					}
				}
				else { 
					var x = a[primary].toLowerCase ? a[primary].toLowerCase() : a[primary], 
					    y = b[primary].toLowerCase ? b[primary].toLowerCase() : b[primary];
					if(this.state.asc) return x < y ? -1 : x > y ? 1 : 0;
					else return x > y ? -1 : x < y ? 1 : 0;
				}
				
			}.bind(this));
			return data;
		}
		return data;
	},
	sort:function() {
		if(this.props.data && this.props.data.length) {
			var data = this.props.data.slice();
			var primary = this.state.primarySort;
			data.sort(function(a,b) {
				if(this.state.asc) {
					if(a[primary]>b[primary]) 	return 1;
					if(a[primary]<b[primary]) 	return -1;
					else						return 0;	
				}
				else {
					if(a[primary]<b[primary]) 	return 1;
					if(a[primary]>b[primary]) 	return -1;
					else						return 0;		
				}
				
			}.bind(this));
			return data;
		}
		return [];
	},
	render:function() {
		var self = this;
		var rows = this.complexSort();
		
		rows = rows.map(function(item) {
			return <tr onMouseOver={this.onMouseOver} >
					<td>{item.siteId}</td>
					<td>{item.catalogId}</td>
					<td>{item.catalogName}</td>
					<td>{item.categoryId}</td>
					<td>{item.categoryName}</td>
				</tr>
		}.bind(this)) || [];



		return <div>
					<Modal buttonTitle="Complex Sorting" onClose={this.onClose} onOpen={this.onOpen}>
						<MySelect ref="primarySelect">
							<option value="">Primary Sort</option>
				            <option value="siteId">Site Id</option>
				            <option value="catalogId">Catalog Id</option>
				            <option value="catalogName">Catalog Name</option>
				            <option value="categoryId">Category Id</option>
						</MySelect>
						<MySelect ref="secondarySelect">
							<option value="">Secondary Sort</option>
							<option value="siteId">Site Id</option>
				            <option value="catalogId">Catalog Id</option>
				            <option value="catalogName">Catalog Name</option>
				            <option value="categoryId">Category Id</option>
						</MySelect>
					</Modal>
					<table className="table table-hover mar-top-15" id="table">
						<thead>
							<tr onClick={this.onClick}>
							   <th id="siteId">Site Id</th>
							   <th id="catalogId">Catalog Id</th>
							   <th id="catalogName">Catalog Name</th>
							   <th id="categoryId">Category Id</th>
							   <th id="categoryName">Category Name</th>
							 </tr>
						</thead>
						<tbody>{rows}</tbody>
					</table>
				</div>
	} 

});


module.exports = Table;