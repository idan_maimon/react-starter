var React = require("react");

module.exports = React.createClass({
     getInitialState: function() {
         return {value: ''}
     },
     change: function(event){
         this.setState({value: event.target.value});
         if(this.props.onChange) this.props.onChange(event.target.value);
     },
     render: function(){
        return(
               <select className="form-control mar-top-15" id="site-select" onChange={this.change} value={this.state.value}>
                  {this.props.children}
               </select>
        );
     }
});

