var $ = require("jquery");

function getProductsFromStream(stream) {
	var arr = stream.split("\n"),products = [],productValues,id,img,title;
	for (var i = 0; i < arr.length; i++) {
		productValues = arr[i].split("\t");
		if(productValues[0] && productValues[1] && productValues[2]) {
			id = productValues[0];
			img = productValues[1];
			title = productValues[2];
			products.push({
				id:id,
				img:img,
				title:title
			});
		}
	};
	return products;
}

module.exports = {
	getCategories:function(catalogId,siteId,callback) {
		// var url = "http://testservice-4378.lvs01.dev.ebayc3.com/catalogmapping/getcatalogmapping/json/"+catalogId+"/"+siteId;
		var url = "http://localhost:3000/getCategories/"+catalogId+"/"+siteId;
		$.getJSON(url,function(data) {
			callback(data);
		});
	},
	getProducts:function(categoryId,siteId,callback) {
		// var url = "http://testservice-4378.lvs01.dev.ebayc3.com/voyager/service.php?category_id="+catalogId+"&siteId="+siteId;
		var url = "http://localhost:3000/getProducts/"+categoryId+"/"+siteId;
		$.ajax({
			url:url,
			dataType:"text",
			success:function(dataText) {
				if(!/error/.test(dataText)) {
					callback(getProductsFromStream(dataText));
				}
				else{
					callback([]);
				}
			},
			error:function() {
				callback([]);
			}
		})
		
	}

}