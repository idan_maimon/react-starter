var React 	 = require("react");
var api   	 = require("./api");
var MySelect = require("./components/select");
var Table = require("./components/table");

var App = React.createClass({
	getInitialState:function() { 
		return {
			data:{},
			siteId:-1,
			catId:-1,
			errors:"",
			products:[]
		}
	},
	onChangeSelect:function(value) {
		this.setState({siteId:value},this.onSubmit);
	},
	onKeyPress:function(e) {
		if(e.key == "Enter") this.onSubmit();
	},
	onSubmit:function() {
		api.getCategories(this.refs.catId.getDOMNode().value || -1,this.state.siteId,function(data){
			if(data.errors.length) {
				this.setState({
					errors:JSON.stringify(data.errors),
					data:{}
				});
			}
			else {
				this.setState({
					data:data.mappingList,
					errors:""
				});	
			}
			
		}.bind(this));
	},

	onRowHover:function(e) {
		var siteId = e.target.parentElement.childNodes[0].textContent;
		var categoryId = e.target.parentElement.childNodes[3].textContent;
		api.getProducts(categoryId,siteId,function(data) {
			this.setState({products:data});
		}.bind(this));
	},
	
	
	render:function () {
		var images = [];
		if(this.state.products.length) {
			images = this.state.products.map(function (item) {
				return <div className="col-sm-5 mar-10 pad-10 border">
							<img src={item.img} title={item.title} />
						</div>
			});
		}
		return <div className="container-fluid">
					<div className="row">
						<div className="col-sm-4">
							<MySelect onChange={this.onChangeSelect}>
								<option value="">Select Site Id</option>
				                <option value="0">0</option>
				                <option value="2">2</option>
				                <option value="3">3</option>
				                <option value="16">16</option>
				                <option value="77">77</option>
				                <option value="-1">All</option>
							</MySelect>
							<input type="text" ref="catId" onKeyPress={this.onKeyPress} className="form-control mar-top-15" placeholder="Type in Catalog Id..." />
							<button className="btn btn-primary mar-top-15" onClick={this.onSubmit}>Fetch</button>
							<Table data={this.state.data} onRowHover={this.onRowHover} />
							<div>{this.state.errors}</div>
						</div>
						<div className="col-sm-7">
							{images}
						</div>
					</div>
					
					
				</div>
	}
});



React.render(<App />,document.getElementById("app")); 
 