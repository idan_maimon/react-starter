var gulp = require('gulp');
var watchify = require('watchify');
var assign = require("object-assign");
var browserify = require('browserify');     // Bundles JS.
var reactify = require('reactify');         // Transforms React JSX to JS.
var source = require('vinyl-source-stream');
var paths = {
  css: ['app/css/*.css'],
  app_js: ['./app/js/app.js']
};
var customOpts = {
  entries: [paths.app_js],
  debug: false,
  transform: [reactify],
  cache: {}, packageCache: {}, fullPaths: true
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts)); 

b.on('update', bundle);

function bundle() {
  return b.bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest('./dist/js/'));
}

gulp.task("js",bundle);

gulp.task('css',function() {
  return gulp.src(paths.css)
    .pipe(gulp.dest('./dist/css'));
});
 
gulp.task('copy', function() {
    gulp.src('app/index.html').pipe(gulp.dest('dist'));
});

gulp.task('watch', function() {
  gulp.watch(paths.css, ['css']);
});
 
gulp.task("bundle",function() {
   browserify("./app/js/app.js")
  .transform(reactify)
  .bundle()
  .pipe(source('app.js'))
  .pipe(gulp.dest('./dist/js/'));
  
});


gulp.task('default', ['copy','css', 'bundle']);

// gulp.task('default', ['copy','watch', 'css', 'js',]);
