process.env.PORT =  process.env.PORT || process.argv[2] || 3000;

var categories = require("./data/categories");
var products = require("./data/products");

var express = require('express'),
    app = express();


app.use(express.static('./dist',{index:"index.html"}));

app.get("/getCategories/:catalogId?/:siteId?",function(req,res) {
	//console.log(req.params.siteId,req.params.catalogId);
	if(!req.params.siteId || !req.params.catalogId) {
		res.json({
			"errors":["Error messages in case of failure"]
		});
		return;
	}
	var filtered = categories.filter(function(item){
		if(req.params.siteId == -1 && req.params.catalogId == -1) {
			return true;
		}
		if(req.params.siteId == -1 && req.params.catalogId != -1) {
			return item.catalogId == req.params.catalogId;
		}
		else if(req.params.catalogId == -1 && req.params.siteId != -1) {
			return item.siteId == req.params.siteId;
		}
		else {
			return item.catalogId == req.params.catalogId && item.siteId == req.params.siteId;
		}
	});

	res.json({
		"invocationId": "0",
	    "responseStatus": "SUCCESS",
	    "errors": [],
	    "mappingList":filtered
	});
});


app.get("/getProducts/:categoryId/:siteId",function(req,res) {
	var filtered = products.filter(function(item){
		return item.categoryId == req.params.categoryId && item.siteId == req.params.siteId;
	});

	// res.send(filtered[0].data);
	res.setHeader('Content-type', 'text/plain');
	res.charset = 'UTF-8';
	if(filtered.length && filtered[0].data) {
		res.write(filtered[0].data);	
	}
	else {
		res.write("error - no products were found");	
	}
	
	res.end();
});

app.listen(process.env.PORT,function(){
	console.log("Listening on port "+process.env.PORT);
});